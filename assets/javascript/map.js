function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -34.397, lng: 150.644 }, // Default center coordinates
        zoom: 15 // Default zoom level
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var userLocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(userLocation);

            var marker = new google.maps.Marker({
                position: userLocation,
                map: map,
                title: 'Your Location'
            });

            var placesService = new google.maps.places.PlacesService(map);
            placesService.nearbySearch({
                location: userLocation,
                radius: 500, // Search radius in meters (adjust as needed)
                type: 'parking'
            }, function (results, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        var placeMarker = new google.maps.Marker({
                            position: results[i].geometry.location,
                            map: map,
                            title: results[i].name
                        });
                    }
                }
            });
        });
    }
}
