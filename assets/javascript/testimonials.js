const testimonialsData = [
    {
        content: "ParkingAdda made my life so much easier. I can find parking spaces hassle-free now. Great service!",
        author: "John Doe 1",
        image: "/assets/images/testimonials/boy1.png"
    },
    {
        content: "I love the convenience ParkingAdda offers. It saves me time and stress. Highly recommended!",
        author: "Jane Smith 2",
        image: "/assets/images/testimonials/boy1.png"
    },
    {
        content: "ParkingAdda's app is intuitive and user-friendly. Finding parking has never been this easy.",
        author: "Michael Johnson 3",
        image: "/assets/images/testimonials/boy1.png"
    },
    // Add more testimonials as needed
    {
        content: "ParkingAdda made my life so much easier. I can find parking spaces hassle-free now. Great service!",
        author: "John Doe 4",
        image: "/assests/"
    },
    {
        content: "I love the convenience ParkingAdda offers. It saves me time and stress. Highly recommended!",
        author: "Jane Smith 5",
        image: "/assets/images/testimonials/boy1.png"
    },
    {
        content: "ParkingAdda's app is intuitive and user-friendly. Finding parking has never been this easy.",
        author: "Michael Johnson 6",
        image: "/assets/images/testimonials/boy1.png"
    },
    {
        content: "ParkingAdda made my life so much easier. I can find parking spaces hassle-free now. Great service!",
        author: "John Doe 4",
        image: "/assets/images/testimonials/boy1.png"
    },
    {
        content: "I love the convenience ParkingAdda offers. It saves me time and stress. Highly recommended!",
        author: "Jane Smith 5",
        image: "/assets/images/testimonials/boy1.png"
    },
    {
        content: "ParkingAdda's app is intuitive and user-friendly. Finding parking has never been this easy.",
        author: "Michael Johnson 6",
        image: "/assets/images/testimonials/boy1.png"
    },
];



const testimonialsSlider = document.querySelector('.testimonials-slider');

function displayTestimonials() {
    testimonialsSlider.innerHTML = ""; // Clear previous testimonials

    for (let i = 0; i < 3; i++) {
        const testimonial = testimonialsData.shift(); // Get and remove the first testimonial from the array
        if (testimonial) {
            const testimonialCard = document.createElement("div");
            testimonialCard.className = "testimonial-card";
            testimonialCard.innerHTML = `
                <div class="testimonial-content">
                    <p class="abtcnt omcnt1">"${testimonial.content}"</p>
                </div>
                <div class="testimonial-author">
                    <img src="${testimonial.image}" alt="${testimonial.author}">
                    <p>${testimonial.author}</p>
                </div>
            `;
            testimonialsSlider.appendChild(testimonialCard);
            testimonialsData.push(testimonial); // Add the testimonial back to the end of the array
        }
    }
}

function slideTestimonials() {
    testimonialsSlider.style.transition = 'transform 0.5s ease-in-out';
    testimonialsSlider.style.transform = 'translateX(-33.333%)'; // Slide left by one testimonial width
    setTimeout(() => {
        testimonialsSlider.appendChild(testimonialsSlider.firstElementChild); // Move the first testimonial to the end
        testimonialsSlider.style.transition = 'none'; // Disable transition
        testimonialsSlider.style.transform = 'translateX(0)'; // Reset slider position
        setTimeout(() => {
            testimonialsSlider.style.transition = 'transform 0.5s ease-in-out'; // Re-enable transition
        });
    }, 500); // Wait for the sliding animation to complete
}

function changeTestimonials() {
    setInterval(() => {
        slideTestimonials();
        setTimeout(displayTestimonials, 500); // Wait for the sliding animation to finish before updating content
    }, 3000); // Change testimonials every 3 seconds
}

// Initial display of testimonials
displayTestimonials();

// Start changing testimonials
changeTestimonials();