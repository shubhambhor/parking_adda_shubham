let currentIndex = 0;
const totalItems = document.querySelectorAll('.carousel-item').length;

function showItem(index) {
    const items = document.querySelector('.carousel');
    items.style.transform = 'translateX(' + (-index * 100) + '%)';
}

function nextItem() {
    currentIndex = (currentIndex + 1) % totalItems;
    showItem(currentIndex);
}

setInterval(nextItem, 4000); // Change slide every 4 seconds (4000 milliseconds)

